const express = require("express");
const cors = require("cors");

// CONTROLLER
//PLANETS
const PlanetsController = require("./controller/PlanetsController");
const planetsController = new PlanetsController();
// PERSON
const PersonController = require("./controller/PersonController");
const personController = new PersonController();

// CONFIG INIT
const app = express();
app.use(express.json());
app.use(cors());
// ROUTES
app.get("/", (_req, res) => res.json({
  routes: ["/planets", "/person"],
  sortByAvaiblesForPerson: {
    sort: ["name", "mass", "height"],
    example: "/person?sortBy=mass",
  },
}));
app.get("/planets", planetsController.getPlanets);
app.get("/person", personController.getPerson);

module.exports = app;
