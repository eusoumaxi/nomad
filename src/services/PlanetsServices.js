const findStarWars = require("../repository");

class PlanetsServices {
  constructor() {
    /**
     * @private get all planets
     */
    this.repository = findStarWars(true);
  }
  async getPlanets() {
    return this.repository;
  }
}
module.exports = PlanetsServices;
