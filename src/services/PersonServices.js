const findStarWars = require("../repository");
const cloneDataMemory = require("../shared/clone");

class PersonServices {
  constructor() {
    /**
     * @private get all planets
     */
    this.repository = findStarWars();
  }
  async getPerson() {
    return cloneDataMemory(await this.repository);
  }
}
module.exports = PersonServices;
