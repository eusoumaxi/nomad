const Axios = require("axios");

const findStarWars = async (isPlanet) => {
  const urls = ["https://swapi.dev/api/people/"];
  if (isPlanet) {
    urls.push("https://swapi.dev/api/planets/");
  }

  const getPage = async (url) => {
    try {
      const { data } = await Axios.default.get(url);
      return data;
    } catch (error) {
      console.log(error);
      return {};
    }
  };

  const getAllPages = async (url, collection = []) => {
    const { results = {}, next } = await getPage(url);
    collection = [...collection, ...results];
    if (next !== null) {
      return getAllPages(next, collection);
    }
    return collection;
  };

  try {
    const [person, planets] = await Promise.all(
      urls.map((url) => getAllPages(url))
    );

    if (isPlanet && planets) {
      return planets.map((value) => ({
        ...value,
        residents: value.residents.map((urlPersonPlanet) => {
          const findPerson = person.find(({ url: urlPerson }) => {
            return (
              urlPerson.replace(/\D/g, "") ===
              urlPersonPlanet.replace(/\D/g, "")
            );
          });

          return (findPerson && findPerson.name) || urlPersonPlanet;
        }),
      }));
    }
    return person
  } catch (error) {
    console.log(error);
    return [];
  }
};

module.exports = findStarWars;
