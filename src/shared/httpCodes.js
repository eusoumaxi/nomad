const OK = 200;
const INTERNAL_ERROR = 500
const BAD_REQUEST = 400
module.exports = {
  OK,
  INTERNAL_ERROR,
  BAD_REQUEST,
};
