const cloneDataMemory = (data = []) => {
  const clone = [];
  data.forEach((v) => clone.push(v));
  return clone;
};
module.exports = cloneDataMemory