const express = require("express");
const Services = require("../services/PersonServices");
const { OK, INTERNAL_ERROR, BAD_REQUEST } = require("../shared/httpCodes");
let services;
class PersonController {
  constructor() {
    /**
     * @private get all services
     */
    services = new Services();
  }
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   */
  async getPerson(req, res) {
    const { sortBy } = req.query;
    const sortByPermitted = ["name", "mass", "height"];
    if (sortBy && !sortByPermitted.includes(sortBy)) {
      return res.status(BAD_REQUEST).send("bad request");
    }
    try {
      const data = await services.getPerson();
      if (data && sortBy) {
        data.sort((a, b) => {
          if (a[sortBy] < b[sortBy]) return -1;
        });
      }
      return res.status(OK).json(data);
    } catch (error) {
      console.log(error);
      return res.status(INTERNAL_ERROR).json({});
    }
  }
}
module.exports = PersonController;
