const express = require("express");
const Services = require("../services/PlanetsServices");
const { OK, INTERNAL_ERROR } = require("../shared/httpCodes");
let services;
class PlanetsController {
  constructor() {
    /**
     * @private get all services
     */
    services = new Services();
  }
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   */
  async getPlanets(_req, res) {
    try {
      const data = await services.getPlanets();
      return res.status(OK).json(data);
    } catch (error) {
      console.log(error);
      return res.status(INTERNAL_ERROR).json({});
    }
  }
}
module.exports = PlanetsController;
